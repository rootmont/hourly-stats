FROM python:3.7

COPY ./common /common
COPY ./hourly /hourly
COPY ./requirements.txt /hourly
WORKDIR /hourly
RUN pip install -r requirements.txt

CMD ["python", "main.py"]